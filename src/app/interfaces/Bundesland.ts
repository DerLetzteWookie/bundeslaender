export interface Bundesland {
    count : number,
    foreignCountry : boolean,
    letter : string,
    name : string,
    schoolCount : number,
    stadtStaat : boolean,
    url : string
}