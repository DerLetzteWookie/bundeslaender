import { Component, OnInit, Input, APP_ID } from '@angular/core';

import { Bundesland } from '../interfaces/Bundesland';

@Component({
  selector: 'bundesland-item',
  templateUrl: './bundesland.component.html',
  styleUrls: ['./bundesland.component.less']
})
export class BundeslandComponent {
    @Input()
    bundesland! : Bundesland;
}
