import { Component, OnInit, Input, OnChanges } from '@angular/core';

import data from './../assets/bundesland.json';

import { Bundesland } from './interfaces/Bundesland';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.less']
})
export class AppComponent {
  title = 'bundeslaender';
  bundeslaender : Bundesland[] = data.stateList ? data.stateList : [];
  //personIndexCounts nicht die eleganteste Lösung. Entstand eher aus der Not. Schönerer Lösungsansatz findet man als Kommentar in app.component.html
  personIndexCounts : Object = data.personIndexCounts ? Object.keys(data.personIndexCounts).filter((letter:string) => data.personIndexCounts[letter as keyof typeof data.personIndexCounts] > 0 ) : [];
  letterFilter : string = "";
  parseInt = parseInt;

  setLetterFilter( letter : any ): void {
    console.log(letter);
    this.letterFilter = letter.toUpperCase();
  }

  ngOnInit() {
    console.log( this.bundeslaender );
  }
}
